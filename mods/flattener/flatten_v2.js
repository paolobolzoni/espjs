'use strict';

const openmwCfg = '/home/paolo/.config/openmw/openmw.cfg';
const outputEspFile = '/home/paolo/games/Morrowind/Data Files Simple Mods/flatten.esp';

// Have the script flatten to the min among the original level and the maxLevel value
// put 1 to have full unlevelled lists
// a value like 4 will make the starting of the game easier
const maxLevel = 1;

// Respect the "calc all levels" flags
// if true when a list does not has the flag only the highest level part of it
// will be kept, and flattened.
const calcAllLevelItems = true;
const calcAllLevelCreatures = false;

// Pass the Item chance none value through this function
// Replace with 'return v;' to just keep the original value
const chanceNoneItem = function(v) {
    // reduces the change of getting nothing
    return Math.max(0, (v-10) * 0.75);
}

// Pass the Creature chance none value through this function
// Replace with 'return v;' to just keep the original value
const chanceNoneCrea = function(v) {
    if (v > 85) {
        // leave very uncommon monsters uncommon
        return v;
    }
    // reduces the change of meeting nobody
    return Math.max(0, (v-7) * 0.9);
}

// Caps the maximum aggressivity of creatures making easier to avoid them
// Fight 83 is like mudcrabs, put 100 to keep untouched
const creatureFightMax = 85;


/// --- program start ---

const libEsp = require('./lib/esp.js');
const fs = require('fs');
const path = require('path');

function sortll(ll) {
    ll.sort(function(l, r) {
        return l[1] - r[1];
    });
}


function flatten(record) {
   if (record.type === "Creature") {
     if (record.ai_data.fight <= creatureFightMax) {
        return null;
     }

     record.ai_data.fight = creatureFightMax;
     return record;
   }

   if (record.type === "LevelledItem") {
       if (record.items.length === 0) {
          return null;
       }
       sortll(record.items);

       let stopAt = -1
       // if calculate all level is off, just keep the tail of the list
       if (calcAllLevelItems && ((record.list_flags&2) === 0)) {
          stopAt = record.items[record.items.length - 1][1]
       }
       let changed = record.list_flags !== (record.list_flags | 2);
       record.list_flags = record.list_flags | 2;

       let chance = ~~chanceNoneItem(record.chance_none);
       changed = changed || chance != record.chance_none;
       record.chance_none = chance;

       record.flags = [ 0, 0 ];
       let nitems = [];
       for (let i = record.items.length; i > 0;) {
           i--;
           let itm = record.items[i];

           if (stopAt !== -1  &&  stopAt !== itm[1]) {
                changed = true;
                break
           }

           let nl = Math.min(maxLevel, itm[1]);
           changed = changed || nl != itm[1];
           itm[1] = nl;
           nitems.unshift(itm);
       }
       if (nitems[0][1] != 1) {
           // fix corner cases where the whole list become higher than level 1
           for (let i = 0; i < nitems.length; ++i) {
              nitems[i][1] = 1;
           }
       }
       record.items = nitems;
       if (changed) return record;
       return null;
   }

   if (record.type === "LevelledCreature") {
       if (record.creatures.length === 0) {
          return null;
       }
       sortll(record.creatures);

       // if calculate all level is off, just keep the tail of the list
       let stopAt = -1
       if (calcAllLevelCreatures && record.list_flags === 0) {
          stopAt = record.creatures[record.creatures.length - 1][1]
       }
       let changed = record.list_flags != 1;
       record.list_flags = 1;

       let chance = ~~chanceNoneCrea(record.chance_none);
       changed = changed || chance != record.chance_none;
       record.chance_none = chance;

       record.flags = [ 0, 0 ];
       let ncreats = [];
       for (let i = record.creatures.length; i > 0;) {
           i--;
           let c = record.creatures[i];

           if (stopAt !== -1  &&  stopAt !== c[1]) {
                changed = true;
                break
           }

           let nl = Math.min(maxLevel, c[1]);
           changed = changed || nl != c[1];
           c[1] = nl
           ncreats.unshift(c);
       }
       if (ncreats[0][1] != 1) {
          for (let i = 0; i < ncreats.length; ++i) {
             ncreats[0][1] = 1;
          }
       }

       record.creatures = ncreats;
       if (changed) return record;
       return null;
   }

   return null;
}

function writeOutput(masters, records) {
    const writer = libEsp.writeEsp(outputEspFile);
    let header = JSON.parse('{ "type": "Header", "flags": [ 0, 0 ], "version": 1.3, "file_type": "Esp", "author": "", "description": "Flattened levelled lists", "num_objects": 1, "masters": [ ] }');
    header.masters = masters;

    writer.add(header);
    records.forEach(writer.add);
    writer.close();
}

function flatAll(data) {
    let records = [];
    let involvedMasters = {};

    for (let k in data.records) {
        let nrecord = flatten(data.records[k]);
        if (nrecord != null) {
            records.push(nrecord);
            involvedMasters[ data.masters[nrecord.id][1]  ] = data.masters[nrecord.id];
        }
    }

    let mastersFull = [];
    for (let k in involvedMasters) {
        mastersFull.push(involvedMasters[k]);
    }
    mastersFull.sort(function(l, r) {
        return l[0] - r[0];
    });

    let masters = [];
    for (let i = 0; i < mastersFull.length; ++i) {
        let m = mastersFull[i];
        masters.push([ m[1], m[2] ])
    }

    writeOutput(masters, records);
}

function operateSingle(data, i) {
    function pass(err, record) {
        if (!!err) {
            console.log('Error:', err);
            return
        }

        if (!!record) {
            if (data.records[record.id] != null || record.type === "Creature" || record.type === "LevelledItem" || record.type === "LevelledCreature") {
                data.records[record.id] = record;
                data.masters[record.id] = [ i, data.mastersSize[i][0], data.mastersSize[i][1] ];
            }
            return reader.get(pass);
        }

        return operateSingle(data, ++i);
    }

    if (i === data.esps.length) {
        data.esps = undefined;
        data.mastersSize = undefined;
        flatAll(data);
        return
    }

    const reader = libEsp.streamEsp(data.esps[i]);
    reader.get(pass);
}

function operate(esps, mastersSize) {
    let data = {
        esps,
        masters: {},
        mastersSize,
        records: {},
    };

    operateSingle(data, 0);
}

function main() {
    function findContentFiles(dirs, names) {
        let esps = [];
        let mastersSize = [];
        for (let n = 0; n < names.length; ++n) {
            let found = false;
            for (let d = 0; d < dirs.length; ++d) {
                const fn = existsCaseInsensitive(dirs[d], names[n]);
                if (!!fn) {
                    const p = path.join(dirs[d], fn);
                    mastersSize.push([ names[n], getFilesizeInBytes(p) ]);
                    esps.push(p);
                    found = true;
                    break;
                }
            }
            if (!found) {
                console.error('Error: cannot find ', names[n]);
                process.exit(5);
            }
        }
        operate(esps, mastersSize)
    }

    const dataRows = [];
    const contentRows = [];
    const allFileContents = fs.readFileSync(openmwCfg, 'utf-8');
    allFileContents.split(/\r?\n/).forEach(function (line) {
        if (!!line.match(/^data=/)) {
            dataRows.push(line.slice(6, -1));
        }
        if (!!line.match(/^content=/)) {
            if (line.endsWith('.esp') || line.endsWith('.esm') || line.endsWith('.omwaddon')) {
                contentRows.push(line.slice(8));
            }
        }
    });
    findContentFiles(dataRows, contentRows);
}

const existsCaseInsensitive = (function(){
    let knownDirs = {}

    return function (dir, filename) {
        let ap = path.resolve(dir);
        if (knownDirs[ap] != null) {
            return knownDirs[ap][ filename.toLowerCase() ];
        }

        let lfs = {};
        let fl = fs.readdirSync(ap);
        for (let n = 0; n < fl.length; ++n) {
            lfs[ fl[n].toLowerCase() ] = fl[n];
        }
        knownDirs[ap] = lfs;

        return lfs[ filename.toLowerCase() ];
    }
}())

function getFilesizeInBytes(filename) {
    let stats = fs.statSync(filename);
    return stats.size;
}

main();
