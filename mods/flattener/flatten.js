'use strict';

const openmwCfg = '/home/paolo/.config/openmw/openmw.cfg';
const outputEspFile = '/home/paolo/games/Morrowind/Data Files Simple Mods/flatten.esp';

// Have the script flatten to the min among the original level and the maxLevel value
// put 1 to have full unlevelled lists
// a value like 4 will make the starting of the game easier
const maxLevel = 1;

// Respect the "calc all levels" flags
// if true when a list does not has the flag only the highest level part of it
// will be kept, and flattened.
const calcAllLevelItems = true;
const calcAllLevelCreatures = false;

// Pass the Item chance none value through this function
// Replace with 'return v;' to just keep the original value
const chanceNoneItem = function(v) {
    // reduces the change of getting nothing
    return Math.max(0, (v-10) * 0.75);
}

// Pass the Creature chance none value through this function
// Replace with 'return v;' to just keep the original value
const chanceNoneCrea = function(v) {
    // reduces the change of meeting nobody
    return Math.max(0, (v-7) * 0.9);
}

// Caps the maximum aggressivity of creatures making easier to avoid them
// Fight 83 is like mudcrabs, put 100 to keep untouched
const creatureFightMax = 85;


/// --- program start ---

const libEsp = require('./lib/esp.js');
const fs = require('fs');
const path = require('path');

function sortll(ll) {
    ll.sort(function(l, r) {
        return l[1] - r[1];
    });
}


function flatten(record, listDone) {
   if (listDone.has(record.id)) {
       return null;
   }

   if (record.type === "Creature") {
     listDone.add(record.id);
     if (record.ai_data.fight <= creatureFightMax) {
        return null;
     }

     record.ai_data.fight = creatureFightMax;
     return record;
   }

   if (record.type === "LevelledItem") {
       listDone.add(record.id);
       if (record.items.length === 0) {
          return null;
       }
       sortll(record.items);

       let stopAt = -1
       // if calculate all level if off, just keep the tail of the list
       if (calcAllLevelItems && ((record.list_flags&2) === 0)) {
          stopAt = record.items[record.items.length - 1][1]
       }
       let changed = record.list_flags != record.list_flags | 2;
       record.list_flags = record.list_flags | 2;

       let chance = ~~chanceNoneItem(record.chance_none);
       changed = changed || chance != record.chance_none;
       record.chance_none = chance;

       record.flags = [ 0, 0 ];
       let nitems = [];
       for (let i = record.items.length; i > 0;) {
           i--;
           let itm = record.items[i];

           if (stopAt !== -1  &&  stopAt !== itm[1]) {
                changed = true;
                break
           }

           let nl = Math.min(maxLevel, itm[1]);
           changed = changed || nl != itm[1];
           itm[1] = nl;
           nitems.unshift(itm);
       }
       if (nitems[0][1] != 1) {
           // fix corner cases where the whole list become higher than level 1
           for (let i = 0; i < nitems.length; ++i) {
              nitems[i][1] = 1;
           }
       }
       record.items = nitems;
       if (changed) return record;
       return null;
   }

   if (record.type === "LevelledCreature") {
       listDone.add(record.id);
       if (record.creatures.length === 0) {
          return null;
       }
       sortll(record.creatures);

       // if calculate all level if off, just keep the tail of the list
       let stopAt = -1
       if (calcAllLevelCreatures && record.list_flags === 0) {
          stopAt = record.creatures[record.creatures.length - 1][1]
       }
       let changed = record.list_flags != 1;
       record.list_flags = 1;

       let chance = ~~chanceNoneCrea(record.chance_none);
       changed = changed || chance != record.chance_none;
       record.chance_none = chance;

       record.flags = [ 0, 0 ];
       let ncreats = [];
       for (let i = record.creatures.length; i > 0;) {
           i--;
           let c = record.creatures[i];

           if (stopAt !== -1  &&  stopAt !== c[1]) {
                changed = true;
                break
           }

           let nl = Math.min(maxLevel, c[1]);
           changed = changed || nl != c[1];
           c[1] = nl
           ncreats.unshift(c);
       }
       if (ncreats[0][1] != 1) {
          for (let i = 0; i < ncreats.length; ++i) {
             ncreats[0][1] = 1;
          }
       }

       record.creatures = ncreats;
       if (changed) return record;
       return null;
   }

   return null;
}

function operateSingle(esps, i, data) {
    function pass(err, record) {
        if (!!err) {
            console.log('Error:', err);
            return data.writer.close();
        }

        if (!!record) {
            record = flatten(record, data.doneIds)
            if (record != null) {
                ++nr;
                data.writer.add(record);
            }
            return reader.get(pass);
        }
        console.log('---->', i, esps[i], "fixed", nr);
        return operateSingle(esps, i, data);
    }

    if (i == 0) {
        return data.writer.close();
    }

    let nr = 0;
    --i;
    const reader = libEsp.streamEsp(esps[i]);
    reader.get(pass);
}

function operate(esps, masters) {
    const writer = libEsp.writeEsp(outputEspFile);
    let header = JSON.parse('{ "type": "Header", "flags": [ 0, 0 ], "version": 1.3, "file_type": "Esp", "author": "", "description": "Flattened levelled lists", "num_objects": 1, "masters": [ ] }');
    header.masters = masters;
    writer.add(header);

    let data = {
        doneIds: new Set(),
        writer,
    };

    operateSingle(esps, esps.length, data);
}

function main() {
    function findContentFiles(dirs, names) {
        let esps = [];
        let masters = [];
        for (let n = 0; n < names.length; ++n) {
            let found = false;
            for (let d = 0; d < dirs.length; ++d) {
                const fn = existsCaseInsensitive(dirs[d], names[n]);
                if (!!fn) {
                    const p = path.join(dirs[d], fn);
                    masters.push([ fn, getFilesizeInBytes(p) ])
                    esps.push(p);
                    found = true;
                    break;
                }
            }
            if (!found) {
                console.error('Error: cannot find ', names[n]);
                process.exit(5);
            }
        }
        operate(esps, masters)
    }

    const dataRows = [];
    const contentRows = [];
    const allFileContents = fs.readFileSync(openmwCfg, 'utf-8');
    allFileContents.split(/\r?\n/).forEach(function (line) {
        if (!!line.match(/^data=/)) {
            dataRows.push(line.slice(6, -1));
        }
        if (!!line.match(/^content=/)) {
            if (line.endsWith('.esp') || line.endsWith('.esm') || line.endsWith('.omwaddon')) {
                contentRows.push(line.slice(8));
            }
        }
    });
    findContentFiles(dataRows, contentRows);
}
main();

function existsCaseInsensitive(dir, filename) {
    let lfs = {};
    let fl = fs.readdirSync(dir);
    for (let n = 0; n < fl.length; ++n) {
        lfs[ fl[n].toLowerCase() ] = fl[n];
    }
    return lfs[ filename.toLowerCase() ];
}

function getFilesizeInBytes(filename) {
    let stats = fs.statSync(filename);
    let fileSizeInBytes = stats.size;
    return fileSizeInBytes;
}
