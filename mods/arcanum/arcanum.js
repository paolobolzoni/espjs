'use strict';

const openmwCfg = '/home/paolo/.config/openmw/openmw.cfg';
const outputEsp = '/home/paolo/games/Morrowind/Data Files Simple Mods/aarcanum.esp';
const specialIcon = true;

// ------- main program

const libEsp = require('./lib/esp.js');
const fs = require('fs');
const path = require('path');
const child_process = require('child_process');


function createScripts(data, record, master) {
    let scriptTemplate = String.raw`{
        "type": "Script",
        "flags": [ 0, 0 ],
        "id": "etbT|title|",
        "header": { "num_shorts": 2, "num_longs": 0, "num_floats": 0, "bytecode_length": 2, "variables_length": 0 },
        "bytecode": "KLUv/QBYMQAAAgAAAAEB",
        "text": "begin etbT|title|\r\n\t; teach == 0 -> nothing to do\r\n\t; teach == 1 -> add spell, destroy scroll\r\n\t; teach == 2 -> activate the scroll to display it\r\n\r\n\tshort teach\r\n\tshort PCSkipEquip\r\n\r\n\tIf ( OnActivate == 1 )\r\n\t\tset teach to 2\r\n\tendIf\r\n\r\n\tif ( PcSkipEquip == 1 )\r\n\t\tset teach to 1\r\n\tendIf\r\n\tset PcSkipEquip to 0\r\n\r\n\tif ( teach == 0 )\r\n\t\treturn\r\n\telseIf ( teach == 2 )\r\n\t\tactivate\r\n\t\tset teach to 0\r\n\t\treturn\r\n\tendIf\r\n\tset teach to 0\r\n\r\n\tplayer->Additem \"key_standard_01\" 1\r\n\tplayer->RemoveItem \"key_standard_01\" 1\r\n\r\n\tif ( player->getSpell \"|id|\" == 1 )\r\n\t\treturn\r\n\tendIf\r\n\r\n\tplayer->RemoveItem \"etbS|title|\" 1\r\n\r\n\tplayer->addSpell \"|id|\"\r\n\tmessageBox \"The Arcanum burns in a cold |color| flame and scorches |name| in your brain.\"\r\nend\r\n"
      }`;
    let enchTemplate = String.raw`{ "type": "Enchantment", "flags": [ 0, 0 ], "id": "etbE|title|", "data": { "enchant_type": "CastOnce", "enchant_cost": 8, "max_charge": 8, "flags": 1 }, "effects": [] }`;

    data.spells = {};
    for (let id of data.spell) {
        let spell = data.records[id];
        master[data.masters[id][0]] = data.masters[id];

        let scriptRecord = JSON.parse(scriptTemplate);

        scriptRecord.text = replaceAll(scriptRecord.text, '\\|id\\|',  spell.id);
        scriptRecord.text = replaceAll(scriptRecord.text, '\\|title\\|', toTitle(spell.id));
        scriptRecord.text = replaceAll(scriptRecord.text, '\\|name\\|', spell.name);
        scriptRecord.text = replaceAll(scriptRecord.text, '\\|color\\|', flameColor[flameColorN]);
        flameColorN = ( flameColorN + 1 ) % flameColor.length;
        scriptRecord.id = replaceAll(scriptRecord.id, '\\|title\\|', toTitle(spell.id));

        if (data.spells[scriptRecord.id] != null) {
            console.error('Duplicate ID!', scriptRecord.id, '<>', data.spells[scriptRecord.id].id, '<>', spell.id);
            process.exit(1);
        }
        data.spells[scriptRecord.id] = spell;

        let enchRecord = JSON.parse(replaceAll(enchTemplate, '\\|title\\|', toTitle(spell.id)));
        enchRecord.effects = spell.effects

        record.push(scriptRecord);
        record.push(enchRecord);
    }
    console.error("Create scripts and enchantments:", data.spell.size);
}

function getInitials(s) {
    s = s.replace(new RegExp(/ ?to| ?of| ?skill/, 'g'), ' ');
    let init = [];
    s = s.toLowerCase();
    s.replace(/(?:^|\s)([a-z])/g, function(_, i) {
        init.push(i);
    })
    if (init.length == 0) {
        return [ 'q', 'q' ];
    }
    if (init.length == 1) {
        s = s.replace(/\s/g, "");
        return [ init[0],  s[0| s.length / 2] ];
    }

    let ll = init.length - 1;
    let sl = init.length - 2;
    return [ init[sl], init[ll] ];
}

function makeScrolls(data, record, masters) {
    let templateBook = String.raw`{ "type": "Book", "flags": [ 0, 0 ], "id": "etbS|title|", "data": { "weight": 0.1, "value": 0, "flags": 1, "skill": "None", "enchantment": 0 }, "name": "|name| Arcanum", "mesh": "m\\text_scroll_01.nif", "icon": "arcanum\\ico_|letters|.dds", "script": "etbT|title|", "text": "<DIV ALIGN=\"center\"><FONT COLOR=\"000000\" SIZE=\"2\" FACE=\"Magic Cards\">|name|</FONT><BR>" }`;
    let templateLL = String.raw`{ "type": "LevelledItem",
        "flags": [ 0, 0 ],
        "id": "etbLarcana",
        "list_flags": 1,
        "chance_none": 0,
        "items": [ ] }`;

    let spell2scroll = {};
    let list = JSON.parse(templateLL);
    for (let id in data.spells) {
        let spell = data.spells[id];
        let title = toTitle(spell.id);

        let one = replaceAll(templateBook, '\\|id\\|',  spell.id);
        let two = replaceAll(one, '\\|title\\|', title);
        let three = replaceAll(two, '\\|name\\|', spell.name);
        let scrollRecord = JSON.parse(three);

        if (specialIcon) {
            let initials = getInitials(spell.name);
            if ( !fs.existsSync( path.join(__dirname, "icons", "arcanum", "ico_" + initials[0] + initials[1] + ".dds")) ) {
                let r = child_process.spawnSync(path.join(__dirname, "make_icons"), initials);
                if (r.status != 0) {
                    process.exit(4);
                }
            }
            scrollRecord.icon = replaceAll(scrollRecord.icon, '\\|letters\\|', initials[0] + initials[1]);
        } else {
            scrollRecord.icon = "m\\tx_scroll_open_01.dds";
        }

        if (scrollRecord.name.length > 30) {
            scrollRecord.name = spell.name;
        }

        scrollRecord.data.value = spell.data.spell_cost * 10;
        scrollRecord.enchanting = 'etbE' + title;
        scrollRecord.text = scrollRecord.text + "<BR><BR><DIV ALIGN=\"center\"><FONT COLOR=\"000000\" SIZE=\"1\" FACE=\"Daedric\">" + magicalScribble[magicalScribbleN] + "</FONT><BR>";
        magicalScribbleN = ( magicalScribbleN + 1 ) % magicalScribble.length;
        spell2scroll[spell.id] = scrollRecord.id;

        record.push(scrollRecord);
        list.items.push([scrollRecord.id, 1])
    }
    console.error("Create scrolls:", data.spell.size);

    record.push(list);
    data.spell2scroll = spell2scroll;
}

function updateSellerNPCs(data, record, master) {
    for (let id of data.npcSeller) {
        let npc = data.records[id];
        master[data.masters[id][0]] = data.masters[id];

        npc.ai_data.services = npc.ai_data.services & ~2048; //no spells
        npc.ai_data.services = npc.ai_data.services | 8; //yes books

        for (let i = 0; i < npc.spells.length; ++i) {
            let sp = npc.spells[i];
            npc.inventory.push([ -1, data.spell2scroll[sp] ]);
        }

        record.push(npc);
    }
    console.error("Update merchant NPCs inventory:", data.npcSeller.size);
}

function fillLL(data, record, master) {
    let number = 0;
    for (const llid of data.levList) {
        let ll = data.records[llid];

        let hasBooks = false;
        for (let i = 0; i < ll.items.length; ++i) {
            if (data.book.has(ll.items[i][0])) {
                hasBooks = true;
                break;
            }
        }

        hasBooks = hasBooks ||
            ll.id.startsWith("random_bandit_") ||
            ll.id.includes('_loot_');

        if (hasBooks) {
            ++number;
            master[data.masters[llid][0]] = data.masters[llid];
            ll.items.unshift([ 'etbLarcana', 1 ]);
            record.push(ll);
        }
    }
    console.error("Update Levelled Lists:", number);
}

function prepareHeaderMaster(masters) {
    let espMasters = [];
    let m = [];
    for (let id in masters) {
        m.push(masters[id]);
    }
    m.sort(function(lhs, rhs) {
        return lhs[2] - rhs[2];
    })
    for (let i = 0; i < m.length; ++i) {
        let mm = m[i];
        espMasters.push( [ mm[0], mm[1] ] );
    }
    return espMasters;
}

function writeOutput(record, masters) {
    console.error("Write output esp file:", outputEsp);

    let header = JSON.parse('{ "type": "Header", "flags": [ 0, 0 ], "version": 1.3, "file_type": "Esp", "author": "", "description": "", "num_objects": 1, "masters": [ ] }');
    header.masters = prepareHeaderMaster(masters);

    let writer = libEsp.writeEsp(outputEsp);
    writer.add(header);
    record.forEach(writer.add);
    writer.close();
}

function createRecords(data) {
    let record = [];
    let masters = {};
    createScripts(data, record, masters);
    makeScrolls(data, record, masters);
    updateSellerNPCs(data, record, masters);
    fillLL(data, record, masters);
    writeOutput(record, masters);
}

function arrangeRecords(data) {
    data.spell = new Set();
    data.npcSeller = new Set();
    data.levList = new Set();
    data.book = new Set();

    for (let id in data.records) {
        let record = data.records[id]
        if (record.type === "Npc"  &&  !!record.ai_data  &&  (record.ai_data.services & 2048) !== 0) {
            data.npcSeller.add(record.id);
            for (let i = 0; i < record.spells.length; ++i) {
                let sp = record.spells[i];
                data.spell.add(sp);
            }
        }
        if (record.type === "LevelledItem") {
            data.levList.add(record.id);
        }
        if (record.type === "Book") {
            data.book.add(record.id);
        }
    }
    createRecords(data);
}

function collectIds(data, idx) {
    let wantedTypes = new Set([ "Book", "Spell", "Npc", "LevelledItem" ]);
    function pass(err, record) {
        if (!!err) {
            console.error(data.esps[idx]);;
            console.error('Error:', err);
            process.exit(3);
        }

        if (!!record) {
            if (data.records[record.id] != null || wantedTypes.has(record.type)) {
                data.records[record.id] = record;
                data.masters[record.id] = [ esp[1], esp[2], idx ];
            }
            return reader.get(pass);
        }

        return collectIds(data, ++idx);
    }
    if (idx == data.esps.length) {
        data.esps = undefined;
        arrangeRecords(data);
        return;
    }

    const esp = data.esps[idx];
    const reader = libEsp.streamEsp(esp[0]);
    reader.get(pass);
}

function findContentFiles(dirs, names) {
    let esps = [];
    for (let n = 0; n < names.length; ++n) {
        let found = false;
        for (let d = 0; d < dirs.length; ++d) {
            const fn = existsCaseInsensitive(dirs[d], names[n]);
            if (!!fn) {
                let p = path.join(dirs[d], fn);
                esps.push( [ p, fn, getFilesizeInBytes(p) ]);
                found = true;
                break;
            }
        }
        if (!found) {
            console.error('Error: cannot find ', names[n]);
            process.exit(5);
        }
    }
    collectIds({
        esps,
        records: {},
        masters: {},
    }, 0)
}

function main() {
    const dataRows = [];
    const contentRows = [];
    const allFileContents = fs.readFileSync(openmwCfg, 'utf-8');

    allFileContents.split(/\r?\n/).forEach(function (line) {
        if (!!line.match(/^data=/)) {
            dataRows.push(line.slice(6, -1));
        }
        if (!!line.match(/^content=/)) {
            if (line.endsWith('.esp') || line.endsWith('.esm') || line.endsWith('.omwaddon')) {
                contentRows.push(line.slice(8));
            }
        }
    });

    findContentFiles(dataRows, contentRows);
}


// ------- support functions
const existsCaseInsensitive = (function(){
    let knownDirs = {}

    return function (dir, filename) {
        let ap = path.resolve(dir);
        if (knownDirs[ap] != null) {
            return knownDirs[ap][ filename.toLowerCase() ];
        }

        let lfs = {};
        let fl = fs.readdirSync(ap);
        for (let n = 0; n < fl.length; ++n) {
            lfs[ fl[n].toLowerCase() ] = fl[n];
        }
        knownDirs[ap] = lfs;

        return lfs[ filename.toLowerCase() ];
    }
}())


function getFilesizeInBytes(filename) {
    let stats = fs.statSync(filename);
    return stats.size;
}

function toTitle(id) {
    return id.replace(/[^a-zA-Z0-9_]/g, '')
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

const randZX81 = (function(){
    var x = 0;
    var a = 75;
    var c = 74;
    var m = 65537;
    return function() {
        x = ( x*a + c ) % m;
        return x / m;
    }
}());

// ------- data

const flameColor = [ 'azure', 'purple', 'rose-colored', 'red', 'blue', 'yellow', 'green', 'rainbow-colored', 'crimson', 'white', 'pink', 'gray' ];
let flameColorN = 0;

const magicalScribble = [ 'occurret similium cognitas parte culpa industria an animum faciendam sim',
'quidem chimaeram dubitare existi. desinerem velim occurrere operatione figura. discrimen nexum quisnam ostendam assidere liberam. sua justam deo nemine percipiat alligatus opera opinantem dum imagines fortassis pergamque habere mearum assidere. mei vulgo conversa propter.',
'fecto succedens famam vetus procuravi numeri supponit. ullius sufficit formalem. nudam putabo negat meditatio minuta originem sacras jactantur sequitur indiciis errores majus. videntur qua omne lapide adsunt typis innumera adesse regula supponant difficilia expectabam colore automata deceptor posuisse. illarum quanta etc perfectum sciamus chimerae conemur conservat.',
'manibus constanter supponatur urgeat clarius satis quid loco sub ullum vice porro familiarem propugnent formari. poni potentiale rationibus aliquibus quid causas color innumera spectentur imaginabar vul occurrere majora agnoscitur religionis faciliorem. praeterea corporeis conceptu vim caligantis credent admovetur longo admitto aeternum distinguo moveri dubitandum parum dum occurrit quolibet. tanta latum figuram parte cap eae iterum verti attendendo emanant sua. eas actu excogitent blandisque idea denique ut.',
'alienas effectu utili judicio tantumque enatare mutata explicetur pensitatis. ultimum occurret an animam impetus impulsum virtutibus tandem totamque differre propugnent saporis. imperiti recordor dare totumque sentiam progressus quod recte appellatur sive continetur fortassis sequeretur visa spectatum. inde possunt scilicet cap dicam nam certus pendeant undenam spectant collabitur quaesita multae conari. geometriam scripto esse credendam colores priusquam perfectius utcunque aliunde sibi alligatus quieti.',
'ferri utrimque easdem commoveo pileos scripti essentiae avocabo fateri cognitione. ideam suo labefactat efficiat ille viderentur ecclesiae nullum solo co probandam conflantur urgeat. purgantur infinite tanti caeteras tractandae exsolvi nego. ferventi omnes volui improviso cognoscam reducantur. certus nos quid istam aequatis sensum majoris.',
'objectioni conari videmur possum abducere quod igitur illa opiniones producatur saltem. calorem certum rationale voces. fuerit illamque conformes fit ineunte similes constat atra artificis potentiali nunquid. animalium nullamque ipsius digna mente effectrix flexibile vestra humano substantia. nonnisi viderentur scioli constet verarum theologos facturum fingam profecta id.',
'scopo invenio novi aliter alteram author figere nonne. durent advertebam utrimque novi formis ideamque. accepi praecipuus naturales callidus percipi supersit innotuit praefatio integram nomine assequi velim. possimus remanet disputari sciam figuram recenseo ceram omnesque materia heri quinimo unitas simplex tanta aspi falsa nego. concipiam hauriantur dubium quin creatis pudeat gnum dividi haud habet soleam dormienti poterunt.',
'una sequor solam essentiae aliquod somnio plerosque multarum minor physicam autem aliud illo sequeretur. novi aliquandiu aliarum induci judicarent deest naturali quaerendum procedere cognitas venientia agi referuntur pendam.',
'fictitium habere meditatas utili cera innumeras actum credent inquiram cognita infigatur referebam.',
'attributa volens nolo negat praecise existat eminenter labefactat diversi simulque ob vitro corporea constare occasione voce ullos. istius profecta externarum exponetur creatis facturum dictis. corporis usitate sufficiunt denegassem vul dormiam ideoque profundum excaecant effectus esse imaginarer curiosius conservant aptum cupio. vitiis mali societati fatigor virorum mansurum interire gallico exponetur conflatum fruebatur corporis naturas. tangitur alia apollonio perspexi recte exhibent.',
'tantas fingi nullas excaecant innumeras ipsi pensitatis aliquo. bonus animos externis hinc item cessarem confirmet expectanti fidam permiscent scire affirmabam manifesta quandiu locis condemnat mirabar. pulses quin existimavi rum maximum potestis pergamque immorari conflantur lectores possum vereorque an essem. ha notaverim manibus dignum delusisse praesenti. substantia diligenter praecipuum apti propria esto.',
'invenero easdem comparem velit singulae complector manifestum habet divelli. expectabam probanda animos cognitio foret. tamque typis venientia indiciis requirunt proponere manum cogitantur soliditas quas. pudeat rationale lor tempore summopere inveniant ventus postquam vapor neutrum maximum judicarint elicitam conari voce contineri cogitatio. assignare falsae invicem via viderunt ausi reliquiae minimum contineat creatum.',
'cumque diversorum libertate ille imagines aeternum nempe perfectior sunt hinc spontaneo albedinem. multis exhibet praecipuis negari lectorum quaslibet impulsum cunctaque mem dumtaxat removendo minus. spectant percurrere infixa remotis flexibile mansurum ferenda longa industria sensus sapere angelos. calida cogitantur praeterea. sub falso fingo praevidere contrarium liberet auditum quomodo ac error clarae optima illamque post quaslibet opera.',
'scio cognitio sunt agnoscitur attigi percipior erroris. refutent judiciis eas visum praefatio rei locis societati usu haberi error evadit attigeram du vitiis sexta similium. respondeam ipsius geometriam meam immensi otii dubitare perspexi recte multae actiones reddendum autho. atheorum denegassem agnoscitur incertas conceptu sanguinem dividi quaesita facilem sufficiunt voluntates communi actum supponatur satyriscos fallit moveri. divinae percipi transferre admonitus continent velim putavi putandum crescit perfectius agendam tractare verumtamen naturae cujuslibet.',
'seipso meas to tantaeque quasi lapide ejusque enatare examinavi ingenio cau purgantur originis aliud annos melius. entis igitur conatus praecipuis ingressus praestare quaeretur externo physicam otii. enatare age similium inquiram agendis cogitans deum ullas sane vulgus infinitam vita dicamne minimum. profecta meipsum nequit. toga ideis hactenus ac utens meum anno.',
'nunquam volent perfectior ullos eo hac urgeat praemissae propugnent fallar sequeretur tactiles. respondere quapropter loquendi possimus nequeam obversari vestra advertisse scriptum. coloribus differant fallere pluribus cau examinavi sentio locum absoluta opportune integram. vestibus corrigatur realem occurrat. vice idearum divelli atra concilium remanetne sese nolim scientiam quascunque facultates primam poterunt admittere formantur talis.',
'persuadeam deesset verumtamen naturas tur qualem. sive talium odor illarum nam mali. meas constet quaero diversitas justa divelli recte fides respondeam pendere habet sacra voluntates. seu animum divinae corporeis varia. assentiri uno ipsi judicarem exerant quovis potest studebunt volent humanum essent nolens importare.',
'ostendam meam rum vulgo adeo. frigus atqui fit operatione lectorum sensuum collabitur applicare. opinio pileos quodcumque pertineant quoque recenseo uno autho dem otii. nec remanetne similibus nusquam rei aspexi utens causis. testari tertia lapide vera odor nunquid juvare teneri conflantur invenio absoluta effecerit dispari novo.',
'potui cessarem bere nemoque religionis digna rem recte tamque facit. objectivam tollentur intueor favis. singulis numeranda infirmari secunda. experior magnum differre fatigor periculi quaerantur brachia automata animalia judicarem nam. cupio potest ipsis imaginandi affirmabam haereo videbuntur.',
'dissimilem potentiali notatu remotam cunctatus existeret arcte devinctam liberam coelum opportune quaerantur suum emanant proprie factum. putarem rantem potentiale ex docetur quicquam occasionem praecipuis reducit scriptis quae certum idem unde colligere figuram. alias autem experimur perceptio animae subjectum capacem nulli. accepi diversum possimus rationale insidias quadrati alicui curantes. ipse lapidem praesertim motum habuerim considerem occurrunt.',
'ostendam voluntates fortassis ad ima addi. imponere facillimam novi replere locis mutationum maxima. perfectius facturum credo fusius quaenam alienum. succedens referebam veras me color facultate aures. ejusdem delaberer tertia tantumque istae opiniones infinite objectiva.',
'cessarem faciendam praeclare distincte objectivam spontaneo proinde igni contineat referam dispari scripti speranda utramque unitas dignum quocunque. cerae meam meas intellectu maxima contineri incipere ab physicae cujus caeteras. humanam earumque effecerit iterum istam plus removendo tanta paulo industria nova cap solam noluisse societati serio. nihilum hesterna videntur confidere putare defectu nec unquam sapere exempli quos agnosco quolibet saporis istiusmodi agi quiddam. scientiis existam confidere.',
'excogitent materia parum foret theologiae ritas mentemque justam exempli recurrunt conservet tractatur tamen nocturna. authorem potuerunt generalia ipsum ope scripturis aër iis procedere opera. infiniti cadavere amen cum quocunque istiusmodi optima majora actione causas opinantem nullas venturum debeat quascunque figura. inferri sonum obstet advertebam assumere exhibent credamus impellit corporeas possumne. me magna spero praecipuis vul nullam quales ulla heri habet divisibile rerumque plures formalem sic perfectius.',
'imo erit nitebatur extensio conari discere occurret vera qualis vos ejusmodi distingui praestare venti postquam discere utramque. ferventi objectivam ibidem immortalem adsit reddere dispari exhibet supra tollitur perficitur velim errandi nihili anima. esse consumerem veritates nullae requiratur inter meum. lectores humanum sentio illos tes timet substantia attendamus reipsa qualem respondeo praemia archimede ingenio mutationum multi caeteras. ineunte utiles possunt prudens quovis sentiens recordor talium infiniti alterius dubitavit suam nullae.',
'manifestam mirum fit perfacile sentiens justa negans magnum serie nolens cogor visa praestare conari omniscium. passim detrahere occasio respondeam sumptum.' ];
let magicalScribbleN = 0;

main();
