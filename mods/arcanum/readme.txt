
Arcana of magic
===============

An arcanum of magic is similar to a regular magical scroll, but in addition of
allowing to cast its spell safely, it also allows its user to learn the spell.

To learn the spell embedded in an Arcanum, pick it up and from your inventory
drag it on your character picture on the left. To just activate the Arcanum is
not enough.

Arcana of magic, just like regular magical scrolls, can be used only once. If
you cast or learn the spell, the arcanum burns out.

In this mod, Arcana of magic replace spell vendors. To buy a spell you need
to buy the relative Arcanum.


Installation
============

It is a esp file and icons, just copy the esp file and the icon
directory in the Data Files directory and activate in the lancher.
Keep it fairly high in the load order, just after your esm files and
base patches.

To merge levelled lists is strongly suggested.

The esp is based on Morrowind, Tribunal, and Bloodmoon esm files. So NPC from
other sources my still be able to sell vanilla spells.

In my gitlab there is the script used to make the mod, if you feel adventerous
you can try to use it to extend the mod all your configuration.

https://gitlab.com/paolobolzoni/espjs/-/tree/main/mods/arcanum?ref_type=heads


Suggested with
==============

This mod is meant to be played togheter with the Tome of Spell Making andx
Hammer of Enchanting, to give magic-oriented characters more tools.

Tome of Spell Making n Hammer of Enchanting
https://www.nexusmods.com/morrowind/mods/49956


Why this mod
============

When playing a fighting-oriented character is always exciting to find a piece
of armor or a weapon that fits your fighting style.

On the other hand magic-oriented characters have their main source of power,
learn new spells, based on Gold. Get enough money, go to the spell merchant,
buy the spell. For example, one can go to the Balmora Temple, meet Llathyno
Hlaalu and Llarara Omayn to buy Mark and Recall. But you won't be able to learn
Mark and Recall from plain adventuring.

Since Arcana appear in loot they allow magic-oriented character to find items
that may increase their power significantly making dungeon hunting and quest
solving potentially as rewarding as for fighters.

Arcana also works as regular magical scroll and have a bit bit of intrinsic
value, so they are also useful to magic-negated characters.
A dumb Barbarian may not care to learn Summon Winged Twilight, but being able
to cast it when a strong enemy is beating him can be a life saver; or in the
worst case just sell it and buy few jugs of Mazte.


Alternatives
============

Here are alternative mods that do a similar job.

Lootable Spellbooks
https://www.nexusmods.com/morrowind/mods/47269

Lootable Spellbooks (Sei's Edit)
https://www.nexusmods.com/morrowind/mods/52897
