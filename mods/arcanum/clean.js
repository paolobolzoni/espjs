'use strict';
const libEsp = require('./lib/esp.js');
// after clicking "Compile All" this script removes all extra scripts from the esp file

function main() {
    function pass(err, record) {
        if (!!err) {
            console.log('Error:', err);
            writer.close();
            return;
        }

        if (!!record) {
            if ( record.type === "Script" && !record.id.startsWith("etb") ) {
                ++skipped;
            } else {
                writer.add(record);
            }
            reader.get(pass);
            return;
        }

        console.log("Skipped", skipped);
        writer.close();
        return;
    }

    let skipped = 0;
    const reader = libEsp.streamEsp('/home/paolo/games/Morrowind/mrhostile/arcanum.esp');
    const writer = libEsp.writeEsp('/home/paolo/games/Morrowind/mrhostile/arcanum2.esp');
    reader.get(pass);
}
main();
