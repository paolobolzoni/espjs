'use strict';
const libEsp = require('./lib/esp.js');

const esmDir = '/home/paolo/games/Morrowind/Data Files';
const masterList = [ 'Morrowind.esm' ];
const referenceList = {};

function main() {
    let readCells = null;
    function readCellsT(err, idx, record) {
        if (!!err) {
            console.log('Error:', err);
            reader.close();
            return;
        }

        if (!!record) {
            if (record.type === "Cell") {
                for (let i = 0; i < record.references.length; ++i) {
                    const ref = record.references[i];
                    const key = String(1+idx) + ',' + ref.refr_index;
                    referenceList[key] = ref;
                }
            }
            reader.get(readCells);
            return;
        }
        readCellsStart(idx + 1);
    }

    function readCellsStart(idx) {
        if (idx == masterList.length) {
            return realWork()
        }

        reader = libEsp.streamEsp(masterList[idx]);
        readCells = function(err, record){
            readCellsT(err, idx, record);
        };
        reader.get(readCells);
    }

    function realWork() {
        function pass(err, record) {
            if (!!err) {
                console.log('Error:', err);
                writer.close();
                reader.close();
                return;
            }
            if (!!record) {
                if (record.type === "Cell") {
                    if (record.references.length == 0) {
                        reader.get(pass);
                        return;
                    }

                    for (let i = 0; i < record.references.length; ++i) {
                        const ref = record.references[i];
                        if (!!ref.deleted) {
                            let originalRef = referenceList[ String(ref.mast_index) + "," + ref.refr_index ];
                            let mid = ref.mast_index;
                            let delValue = ref.deleted;

                            record.references[i] = originalRef;
                            record.references[i].mast_index = mid;
                            record.references[i].translation = [0,0,0];
                            record.references[i].rotation = [0,0,0];
                            delete record.references[i].scale;
                            record.references[i].deleted = delValue;
                        }
                    }
                }

                writer.add(record);
                reader.get(pass);
                return;
            }

            writer.close();
        }

        const reader = libEsp.streamEsp('/home/paolo/games/Morrowind/mods/forested/00 Core/Forested morrowind.esp');
        const writer = libEsp.writeEsp('/home/paolo/games/Morrowind/mods/forested/00 Core/Forested morrowind clean.esp');

        reader.get(pass);
    }

    process.chdir(esmDir);
    let reader = null;
    readCellsStart(0);
}


main();
