'use strict';
const libEsp = require('./lib/esp.js');

let filtered_regions = new Set([
    "Ashlands Region",
    "Molag Mar Region",
    "Red Mountain Region"
])


function is_left(a, b, c){
     return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0;
}


function is_yslice(a, b, c) {
    return c.y < Math.max(a.y, b.y)
        && c.y >= Math.min(a.y, b.y)
        && is_left(a,b,c)
}


function filter_wg_extra_trees(ref) {
    let pos = {
        x: ref.translation[0],
        y: ref.translation[1]
    }
    return !(
        (pos.x > -11000 && pos.y > 29600 && pos.y < 42800)
        || (pos.x > -2450 && pos.y > 26300)
        || (pos.x > -51650 && pos.y > 136400)
        || (pos.x > -44000 && pos.y > 116000)
        || (pos.x > -46800 && pos.y > 76700 && pos.y < 95100)
        || (pos.x > -38000 && pos.y > 60900 && pos.y < 70000)
        || (is_left({x: -32050, y: 47000}, {x: -23100, y: 42660}, pos) && pos.x > -34650 && pos.y < 70000)
        || is_yslice({x: -3500, y: 9700}, {x: -5700, y: -2800}, pos)
        || is_yslice({x: -5700, y: -2800}, {x: -2500, y: -6700}, pos)
        || is_yslice({x: -2500, y: -6700}, {x: -6200, y: -21100}, pos)
        || is_yslice({x: -6200, y: -21100}, {x: -24700, y: -40400}, pos)
        || is_yslice({x: -24700, y: -40400}, {x: -19600, y: -45800}, pos)
    ) 
}


function filter_ai_extra_trees(ref) {
    let pos = { 
        x: ref.translation[0],
        y: ref.translation[1]
    }
    return !(
            (pos.x > 25750 && pos.x < 35750 && pos.y > -27600 && pos.y < -17600)
        || ((
            is_yslice({x: -20238, y: -45000}, {x: -15400, y: -38400}, pos)
            || is_yslice({x: -15400, y: -38400}, {x: -0, y: -24500}, pos)
        ) && (
            is_yslice({x: -3500, y: 9700}, {x: -5700, y: -2800}, pos)
            || is_yslice({x: -5700, y: -2800}, {x: -2500, y: -6700}, pos)
            || is_yslice({x: -2500, y: -6700}, {x: -6200, y: -21100}, pos)
            || is_yslice({x: -6200, y: -21100}, {x: -24700, y: -40400}, pos)
            || is_yslice({x: -24700, y: -40400}, {x: -19600, y: -45800}, pos)
    )))
}


function filter_cell_ref(cell, filter) {
    const old_refs = cell.references;
    cell.references = [];
    for (let i = 0; i < old_refs.length; ++i) {
        const ref = old_refs[i];
        if (ref.deleted != null || filter(ref)) {
            cell.references.push(ref);
        }
    }
    if (cell.references.length > 0) {
        return cell;
    }
    return null;
}


function filter(record) {
    if (record.type === "Sound" || record.type === "Book" || record.type === "Dialogue" || record.type === "Info") {
        return null;
    }

    if (record.type !== "Cell" && record.type !== "Region") {
        return record;
    }

    if (record.type === "Region") {
        if(filtered_regions.has(record.id)) {
            return null;
        }
        return record;
    }
    // only Cell records from here

    if (filtered_regions.has(record.region)) {
        return null;
    }

    if (record.region === "Sheogorad") {
        // Keep only the Dagon Fel tree
        if (record.data.grid[0] === 6 && record.data.grid[1] === 20)
            return record
        return null
    }

    if (record.region === "Azura's Coast Region") {
        // Keep only the new trees near the Grazeland
        if (record.data.grid[1] >= 5)
            return record
        return null
    }

    if (record.region === "West Gash Region") {
        // Fix Ashlands borders and Ghostgate gray valley
        return filter_cell_ref(record, filter_wg_extra_trees)
    }

    if (record.region === "Ascadian Isles Region") {
        // Fix Ghostgate gray valley and Molag Amur border
        return filter_cell_ref(record, filter_ai_extra_trees)
    }

    return record;
}


function main() {
    function pass(err, record) {
        if (!!err) {
            console.log('Error:', err);
            writer.close();
            return;
        }

        if (!!record) {
            let newRecord = filter(record);
            if (newRecord != null) {
                writer.add(newRecord);
            }

            reader.get(pass);
            return;
        }

        writer.close();
        return;
    }

    const reader = libEsp.streamEsp('/home/paolo/games/morrowind/mods/forested/00 Core/Forested morrowind.esp');
    const writer = libEsp.writeEsp('/home/paolo/games/morrowind/mods/forested_lf/00 Core/Forested morrowind.esp');
    reader.get(pass);
}

main();

