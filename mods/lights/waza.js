'use strict';

const openmwCfg = '/home/paolo/.config/openmw/openmw.cfg';
const outputEspFile = '/home/paolo/games/Morrowind/Data Files Simple Mods/lights.esp';

const libEsp = require('./lib/esp.js');
const fs = require('fs');
const path = require('path');

const executors = {
    "negative": function(light) {
        if (light.mesh !== "") {
            return null;
        }
        light.data.color = [0, 0, 0, 0];
        light.data.radius = 0;
        return light;
    },
    "colored": function(light) {
        light.data.radius = ~~(1.1 * light.data.radius);

        let hsv = lightHSV(light);

        hsv.s = ~~(hsv.s * 0.9)
        hsv.v = ~~(hsv.v * 0.7)

        return setLightHSV(light, hsv);
    },
    "general": function(light) {
        light.data.flags = light.data.flags & (~(0x48));
        light.data.radius = 2 * light.data.radius;

        let hsv = lightHSV(light);

        hsv.h = ~~(hsv.h * 0.62)
        hsv.s = ~~(hsv.s * 0.8)
        hsv.v = ~~(hsv.v * 0.57)

        return setLightHSV(light, hsv);
    },
}

function getCategory(light) {
    if (light.data.flags & 0x4) {
        return "negative";
    }

    if (light.mesh === '') {
        return "nomesh";
    }

    let hsv = lightHSV(light);
    if (hsv.v > 64 || hsv.v < 14) {
        return "colored";
    }
    return "general";
}

function arrangeRecords(data) {
    let out = {
        master: {},
        record: [],
    };

    // data.records[recordId] -> record;
    // data.masters[recordId] -> [ master name, master size, master idx ];
    for (let id in data.records) {
        let light = data.records[id];
        if (light.type !== "Light") {
            continue;
        }
        let olight = null;

        let c = getCategory(light);
        let f = executors[c];
        if (f != null) {
            olight = f(light);
        }
        if (olight != null) {
            out.master[ data.masters[id][0] ] = data.masters[id];
            out.record.push(olight);
        }
    }
    return out;
}

function writeOutput(out) {
    const writer = libEsp.writeEsp(outputEspFile);
    const header = JSON.parse('{ "type": "Header", "flags": [ 0, 0 ], "version": 1.3, "file_type": "Esp", "author": "", "description": "", "num_objects": 1, "masters": [ [ "Morrowind.esm", 79837557 ] ] }');
    header.masters = prepareHeaderMaster(out.master);

    writer.add(header);
    out.record.forEach(writer.add);
    writer.close();
}

function collectIds(data, idx) {
    function pass(err, record) {
        if (!!err) {
            console.log(data.esps[idx]);;
            console.log('Error:', err);
            process.exit(3);
        }

        if (!!record) {
            if (data.records[record.id] != null || record.type === "Light") {
                data.records[record.id] = record;
                data.masters[record.id] = [ esp[1], esp[2], idx ];
            }
            return reader.get(pass);
        }

        return collectIds(data, ++idx);
    }

    if (idx == data.esps.length) {
        data.esps = undefined;
        let out = arrangeRecords(data);
        writeOutput(out);
        return;
    }

    const esp = data.esps[idx];
    const reader = libEsp.streamEsp(esp[0]);
    reader.get(pass);
}


function findContentFiles(dirs, names) {
    let esps = [];
    for (let n = 0; n < names.length; ++n) {
        let found = false;
        for (let d = 0; d < dirs.length; ++d) {
            const fn = existsCaseInsensitive(dirs[d], names[n]);
            if (!!fn) {
                let p = path.join(dirs[d], fn);
                esps.push( [ p, fn, getFilesizeInBytes(p) ]);
                found = true;
                break;
            }
        }
        if (!found) {
            console.error('Error: cannot find ', names[n]);
            process.exit(5);
        }
    }
    collectIds({
        esps,
        records: {},
        masters: {},
    }, 0)
}


function main() {
    const dataRows = [];
    const contentRows = [];
    const allFileContents = fs.readFileSync(openmwCfg, 'utf-8');

    allFileContents.split(/\r?\n/).forEach(function (line) {
        if (!!line.match(/^data=/)) {
            dataRows.push(line.slice(6, -1));
        }
        if (!!line.match(/^content=/)) {
            if (line.endsWith('.esp') || line.endsWith('.esm') || line.endsWith('.omwaddon')) {
                contentRows.push(line.slice(8));
            }
        }
    });

    findContentFiles(dataRows, contentRows);
}

const existsCaseInsensitive = (function(){
    let knownDirs = {}

    return function (dir, filename) {
        let ap = path.resolve(dir);
        if (knownDirs[ap] != null) {
            return knownDirs[ap][ filename.toLowerCase() ];
        }

        let lfs = {};
        let fl = fs.readdirSync(ap);
        for (let n = 0; n < fl.length; ++n) {
            lfs[ fl[n].toLowerCase() ] = fl[n];
        }
        knownDirs[ap] = lfs;

        return lfs[ filename.toLowerCase() ];
    }
}())

function prepareHeaderMaster(masters) {
    // take js object like data.masters and return the master list ready to
    // write the output header

    let espMasters = [];
    let m = [];
    for (let id in masters) {
        m.push(masters[id]);
    }
    m.sort(function(lhs, rhs) {
        return lhs[2] - rhs[2];
    })
    for (let i = 0; i < m.length; ++i) {
        let mm = m[i];
        espMasters.push( [ mm[0], mm[1] ] );
    }
    return espMasters;
}

function getFilesizeInBytes(filename) {
    let stats = fs.statSync(filename);
    return stats.size;
}

function lightHSV(light) {
    let rgb = {};
    rgb.r = light.data.color[0];
    rgb.g = light.data.color[1];
    rgb.b = light.data.color[2];
    return rgb2hsv(rgb);
}

function setLightHSV(light, hsv) {
    let rgb = hsv2rgb(hsv);
    light.data.color[0] = rgb.r;
    light.data.color[1] = rgb.g;
    light.data.color[2] = rgb.b;
    return light;
}

// input: h in [0,360] and s,v in [0,100] - output: r,g,b in [0,255]
function hsv2rgb(hsv) {
  let h =  hsv.h;
  let s =  hsv.s / 100;
  let v =  hsv.v / 100;

  const f = function( n, k = (n+h/60)%6 ) { return v - v*s*Math.max( Math.min(k,4-k,1), 0); }
  return { r: Math.round(255 * f(5)), g: Math.round(255 * f(3)), b: Math.round(255 * f(1)) };
}

// input: r,g,b in [0,100], out: h in [0,360) and s,v in [0,100]
function rgb2hsv(rgb) {
  let r = rgb.r / 255;
  let g = rgb.g / 255;
  let b = rgb.b / 255;

  let v = Math.max(r,g,b);
  let c = v - Math.min(r,g,b);

  let h = c && ((v==r) ? (g-b)/c : ((v==g) ? 2+(b-r)/c : 4+(r-g)/c));

  return{ h: Math.round(60*(h<0?h+6:h)), s: Math.round(100 * (v&&c/v)), v: Math.round(v * 100) };
}

main();
