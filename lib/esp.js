'use strict';

module.exports = {
    streamEsp,
    writeEsp,
    compress,
    decompress,
    readVertexHeight,
    writeVertexHeight,
}

const cp = require('child_process');
const settings = require('./settings.js');

function streamToString(stream, f) {
  const chunks = [];
  stream.on('data', function(chunk) { chunks.push(Buffer.from(chunk)); });
  stream.on('error', function(err) { f(err); });
  stream.on('end', function() { f(null, Buffer.concat(chunks).toString('utf8')); });
}

function nop(){}


function readVertexHeight(base64buffer, f) {
    function convertData(err, buffer) {
        if (!!err) {
            f(err);
        }

        let vhgt = {};
        vhgt.cell_height = buffer.readFloatLE(0);
        vhgt.height = {};
        for (let y = 0; y < 65; ++y) {
            for (let x = 0; x < 65; ++x) {
                vhgt.height[ [x,y] ] = buffer.readInt8(4 + x + y * 65);
            }
        }

        return f(null, vhgt);
    }
    decompress(base64buffer, convertData);
}


function writeVertexHeight(vhgt, f) {
    let tes3convVhgt = Buffer.alloc(4232);
    tes3convVhgt.writeFloatLE(vhgt.cell_height, 0);
    for (let y = 0; y < 65; ++y) {
        for (let x = 0; x < 65; ++x) {
            tes3convVhgt.writeInt8(vhgt[ [x,y] ], 4 + x + y * 65);
        }
    }
    compress(tes3convVhgt, f);
}


function streamEsp(espFile) {
    let liveTes3conv = cp.spawn(settings.tes3conv, ['-c', espFile]);
    streamToString(liveTes3conv.stderr, function(_, message) {
        errMsg = message;
    });

    let errMsg = '';
    let done = false;

    let foundObj = null;
    let currentObj = [];

    let chunk;
    let idx = 0;

    var currentf = findFirst;
    var level = 0;
    var bar = false;

    function findFirst(c) {
        if (c > 0x7F) {
            currentf = nop;
            errMsg = "Got non-ascii-7 char in the beginning";
            return;
        }
        let cc = String.fromCharCode(c);

        if(/\s|\r|\n/.test(cc)) {
            //
        } else if (cc == '[') {
            currentf = findOCB;
        } else {
            currentf = nop;
            errMsg = "The first non-space char expected to be a [";
            return;
        }
    }

    function findOCB(c) {
        if (c > 0x7F) {
            currentf = nop;
            errMsg = "Got non-ascii-7 char outside objects definition";
            return
        }
        let cc = String.fromCharCode(c);

        if(/\s|\r|\n/.test(cc)) {
            //
        } else if (cc == '{') {
            currentObj.push(c);
            level += 1;
            currentf = findBalanced;
        } else {
            currentf = nop;
            errMsg = "Got unexpected char between objects (wanted { got " + cc + ")";
        }
    }

    function findBalanced(c) {
        let cc = null;
        if (c <= 0x7F) {
            cc = String.fromCharCode(c);
        }

        currentObj.push(c);
        if (cc === '"') {
            currentf = readString;
        } else if (cc === '{') {
            level += 1;
        } else if (cc === '}') {
            level -= 1;
            if (level === 0) {
                foundObj = JSON.parse(Buffer.from(currentObj).toString());
                currentObj.length = 0
                currentf = findNextCommaOrEnd
            }
        }
    }

    function findNextCommaOrEnd(c) {
        let cc = null;
        if (c <= 0x7F) {
            cc = String.fromCharCode(c);
        }

        if(/\s|\r|\n/.test(cc)) {
            //
        } else if (cc === ',') {
            currentf = findOCB;
        } else if (cc === ']') {
            currentf = nop;
        } else {
            currentf = nop;
            errMsg = "Got unexpected char between objects (wanted ] or , got " + cc + ")";
            return;
        }
    }

    function readString(c) {
        let cc = null;
        if (c <= 0x7F) {
            cc = String.fromCharCode(c);
        }

        currentObj.push(c);
        if (cc === '\\' && !bar) {
            bar = true;
        } else if (cc === '"' && !bar) {
            currentf = findBalanced;
        } else if (bar) {
            bar = false;
        }
    }

    liveTes3conv.stdout.once('end', function(){ done = true; });

    function read(f) {
        while (true) {
            if (foundObj != null) {
                let obj = foundObj;
                foundObj = null;
                return f(null, obj);
            }

            if (errMsg !== '') {
                return f({ message: errMsg });
            }

            if (done) {
                return f(null, null);
            }

            if (chunk == null) {
                idx = 0;
                chunk = liveTes3conv.stdout.read();
                if (chunk == null) {
                    liveTes3conv.stdout.once('readable', function(){ onReadable(f); });
                }
                return;
            }

            if (idx < chunk.length) {
                currentf(chunk[idx++]);
            } else {
                chunk = null;
                idx = 0;
            }
        }
    }


    function onReadable(f) {
        chunk = liveTes3conv.stdout.read();
        done = chunk == null;

        read(f);
    }

    return { get: read };
}



function writeEsp(espFile, f) {
    function display_result(err, nrRecords) {
        if (!!err) { 
            console.log('Error:', err); 
            return;
        }
        console.log("Record:", nrRecords);
    }
    if (f == null) {
        f = display_result;
    }

    let nrRecords = 0;
    let errMsg = '';
    let liveTes3conv = cp.spawn(settings.tes3conv, ['-', espFile]);
    streamToString(liveTes3conv.stderr, function(_, message) {
        errMsg = message;
    });

    liveTes3conv.stdin.write('[');

    liveTes3conv.on('close', function(code) {
        add = nop;
        close = nop;
        if (code !== 0) {
            f({ code, message: errMsg });
            return;
        }
        f(null, nrRecords);
        return;
    });

    let add = function(record) {
        if (nrRecords !== 0) {
            liveTes3conv.stdin.write(',');
        }
        ++nrRecords;
        liveTes3conv.stdin.write(JSON.stringify(record));
    }

    let close = function() {
        liveTes3conv.stdin.write(']');
        liveTes3conv.stdin.end();
    }

    return {
        add,
        close,
    }
}



function compress(buffer, f)  {
    let compressed = [];
    let errMessage='';

    const zstd = cp.spawn(settings.zstd);
    streamToString(zstd.stderr, function(_, message) {
        errMessage = message;
    });

    zstd.stdin.write(buffer);
    zstd.stdin.end();

    zstd.stdout.on('readable', function() {
        const nextRead = zstd.stdout.read();
        if (nextRead != null) {
            compressed.push(nextRead);
        }
    });

    zstd.on('close', function(code) {
        if (errMessage !== '') {
            f({ code, error: errMessage })
            return;
        }

        f(null, Buffer.concat(compressed).toString('base64'));
        return;
    });
}



function decompress(data, f)  {
    let uncompressed = [];
    let errMessage='';

    const zstd = cp.spawn(settings.zstd, ['-d']);
    streamToString(zstd.stderr, function(_, message) {
        errMessage = message;
    });

    zstd.stdin.write(Buffer.from(data, 'base64'));
    zstd.stdin.end();

    zstd.stdout.on('readable', function() {
        const nextRead = zstd.stdout.read();
        if (nextRead != null) {
            uncompressed.push(nextRead);
        }
    });

    zstd.on('close', function(code) {
        if (errMessage !== '') {
            f({ code, message: errMessage });
            return;
        }

        f(null, Buffer.concat(uncompressed));
        return;
    });
}
