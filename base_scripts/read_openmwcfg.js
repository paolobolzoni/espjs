'use strict';

const openmwCfg = '/home/paolo/.config/openmw/openmw.cfg';
const outputEspFile = '';


const libEsp = require('./lib/esp.js');
const fs = require('fs');
const path = require('path');


function prepareHeaderMaster(masters) {
    // take js object like data.masters and return the master list ready to
    // write the output header

    let espMasters = [];
    let m = [];
    for (let id in masters) {
        m.push(masters[id]);
    }
    m.sort(function(lhs, rhs) {
        return lhs[2] - rhs[2];
    })
    for (let i = 0; i < m.length; ++i) {
        let mm = m[i];
        espMasters.push( [ mm[0], mm[1] ] );
    }
    return espMasters;
}

function arrangeRecords(data) {
    // data.records[recordId] -> record;
    // data.masters[recordId] -> [ master name, master size, master idx ];
}

function collectIds(data, idx) {
    // extract by type
    let wantedTypes = new Set([ "Book", "Spell", "Npc", "LevelledItem" ]);

    function pass(err, record) {
        if (!!err) {
            console.log(data.esps[idx]);;
            console.log('Error:', err);
            process.exit(3);
        }

        if (!!record) {
            if (data.records[record.id] != null || wantedTypes.has(record.type)) {
                data.records[record.id] = record;
                data.masters[record.id] = [ esp[1], esp[2], idx ];
            }
            return reader.get(pass);
        }

        return collectIds(data, ++idx);
    }
    if (idx == data.esps.length) {
        data.esps = undefined;
        arrangeRecords(data);
        return;
    }

    const esp = data.esps[idx];
    const reader = libEsp.streamEsp(esp[0]);
    reader.get(pass);
}


function findContentFiles(dirs, names) {
    let esps = [];
    for (let n = 0; n < names.length; ++n) {
        let found = false;
        for (let d = 0; d < dirs.length; ++d) {
            const fn = existsCaseInsensitive(dirs[d], names[n]);
            if (!!fn) {
                let p = path.join(dirs[d], fn);
                esps.push( [ p, fn, getFilesizeInBytes(p) ]);
                found = true;
                break;
            }
        }
        if (!found) {
            console.error('Error: cannot find ', names[n]);
            process.exit(5);
        }
    }
    collectIds({
        esps,
        records: {},
        masters: {},
    }, 0)
}


function main() {
    const dataRows = [];
    const contentRows = [];
    const allFileContents = fs.readFileSync(openmwCfg, 'utf-8');

    allFileContents.split(/\r?\n/).forEach(function (line) {
        if (!!line.match(/^data=/)) {
            dataRows.push(line.slice(6, -1));
        }
        if (!!line.match(/^content=/)) {
            if (line.endsWith('.esp') || line.endsWith('.esm') || line.endsWith('.omwaddon')) {
                contentRows.push(line.slice(8));
            }
        }
    });

    findContentFiles(dataRows, contentRows);
}

const existsCaseInsensitive = (function(){
    let knownDirs = {}

    return function (dir, filename) {
        let ap = path.resolve(dir);
        if (knownDirs[ap] != null) {
            return knownDirs[ap][ filename.toLowerCase() ];
        }

        let lfs = {};
        let fl = fs.readdirSync(ap);
        for (let n = 0; n < fl.length; ++n) {
            lfs[ fl[n].toLowerCase() ] = fl[n];
        }
        knownDirs[ap] = lfs;

        return lfs[ filename.toLowerCase() ];
    }
}())

function getFilesizeInBytes(filename) {
    let stats = fs.statSync(filename);
    return stats.size;
}

main();
