'use strict';
const libEsp = require('./lib/esp.js');

function main() {
    function pass(err, record) {
        if (!!err) {
            console.log('Error:', err);
            writer.close();
            return;
        }

        if (!!record) {
            //do something
            reader.get(pass);
            return;
        }

        writer.close();
        return;
    }

    const reader = libEsp.streamEsp('/home/paolo/games/morrowind/mods/forested/00 Core/Forested morrowind.esp');
    const writer = libEsp.writeEsp('/home/paolo/games/morrowind/mods/forested_lf/00 Core/Forested morrowind.esp');
    // writer.add(JSON.parse('{ "type": "Header", "flags": [ 0, 0 ], "version": 1.3, "file_type": "Esp", "author": "", "description": "", "num_objects": 1, "masters": [ [ "Morrowind.esm", 79837557 ] ] }'));
    reader.get(pass);
}


main();
