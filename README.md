# espjs

Little js library to edit tes3 esp/esm files as javascript objects


The heavy lifting of reading and writing esp files is made by Greatness7 tool
tes3conv.

This library reads tes3conv output or feed tes3conv data to have a simple
interface to the esp format.

# to use

edit `settings.js` so that the paths point to the tes3conv executable for now,
ignore the zstd.

Once done requiring `esp.js` you have access to `streamEsp` and `writeEsp`
whose give you a streaming interface to the opened esp file.

Check the example to see how to use.
