'use strict';

const libEsp = require('./lib/esp.js');
process.chdir(__dirname);

function main() {
    const reader = libEsp.streamEsp('/full/path/to/Morrowind/Data Files/Morrowind.esm');
    const writer = libEsp.writeEsp('./noashtree.esp');

    //writer adds the header as first record
    writer.add(JSON.parse('{ "type": "Header", "flags": [ 0, 0 ], "version": 1.3, "file_type": "Esp", "author": "", "description": "", "num_objects": 1, "masters": [ [ "Morrowind.esm", 79837557 ] ] }'));

    function manage_record(err, record) {
        //the callback has two arguments: error and the record
        // if both are null the process is ended
        if (!!err) {
            console.log(err);
            writer.close();
            return;
        }

        if (!!record) {
            //we care only to external cell
            if (record.type === "Cell" && (record.data.flags & 0x01) === 0) {
                const new_references = [];
                //we pass all cell references
                for (let i = 0; i < record.references.length; ++i) {
                    const ref = record.references[i];

                    //if it is the kind we want
                    if (ref.id.toLowerCase().startsWith('flora_ashtree_')) {
                        //we resets its data
                        ref.mast_index = 1; //there is only one master Morrowind.esm, so 1
                        ref.translation = [ 0.0, 0.0, 0.0 ]; //empty
                        ref.rotation = [ 0.0, 0.0, 0.0 ]; //empty
                        delete ref.scale; //totally removed
                        ref.deleted = 4729956; //now is deleted
                        new_references.push(ref);
                    }
                }
                //if we changed anything in the cell, we copy the cell in the output file
                if (new_references.length > 0) {
                    record.references = new_references;
                    writer.add(record);
                }
            }

            reader.get(manage_record);
            return;
        }

        //if both error and record are null, it is over and we close the output file
        writer.close();
        console.log("Done");
    }

    // reader.get get expects a callback
    reader.get(manage_record);
}
main();

